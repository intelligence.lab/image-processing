import java.awt.Color;

public class LogSearch {

	int[][] referenceImagePixels;
	int[][] searchImagePixels;
	double[][] correlationImageArray;
	int refimg_Height;
	int refimg_Width;
	int srchimg_Height;
	int srchimg_Width;
	int currentHeight;
	int currentWidth;
	int currentMidX;
	int currentMidY;

	public LogSearch(int[][] refImgPixels, int[][] srchImgPixels) {

		referenceImagePixels = refImgPixels.clone();
		searchImagePixels = srchImgPixels.clone();
		refimg_Height = referenceImagePixels.length;
		refimg_Width = referenceImagePixels[0].length;
		srchimg_Height = searchImagePixels.length;
		srchimg_Width = searchImagePixels[0].length;

		int k = (int) Math.ceil(Math.log(srchimg_Height) / Math.log(2));
		currentHeight = srchimg_Height / 4;
		currentWidth = srchimg_Width / 4;
		currentMidX = srchimg_Width / 2;
		currentMidY = srchimg_Height / 2;

		double currentCR;

		int tempMidX = currentMidX;
		int tempMIdY = currentMidY;

		CorrTempMatcher cr = new CorrTempMatcher(referenceImagePixels,
				searchImagePixels);
		double maxCorrelation = cr.getCorrelationValAt(currentMidX, currentMidY);
		PlotOutput output = new PlotOutput(referenceImagePixels, searchImagePixels);
		while (k > 0) {
			// north
			currentCR = cr.getCorrelationValAt(currentMidX, currentMidY
					- currentHeight);
			if (currentCR > maxCorrelation) {
				tempMidX = currentMidX;
				tempMIdY = currentMidY - currentHeight;
				maxCorrelation = currentCR;
				output.setPixelBold(tempMidX, tempMIdY, Color.green.getRGB());
			}

			// north east
			currentCR = cr.getCorrelationValAt(currentMidX + currentWidth,
					currentMidY - currentHeight);
			if (currentCR > maxCorrelation) {
				tempMidX = currentMidX + currentWidth;
				tempMIdY = currentMidY - currentHeight;
				maxCorrelation = currentCR;
				output.setPixelBold(tempMidX, tempMIdY, Color.green.getRGB());
			}
			// east
			currentCR = cr.getCorrelationValAt(currentMidX + currentWidth,
					currentMidY);
			if (currentCR > maxCorrelation) {
				tempMidX = currentMidX + currentWidth;
				tempMIdY = currentMidY;
				maxCorrelation = currentCR;
				output.setPixelBold(tempMidX, tempMIdY, Color.green.getRGB());
			}
			// east south
			currentCR = cr.getCorrelationValAt(currentMidX + currentWidth,
					currentMidY + currentHeight);
			if (currentCR > maxCorrelation) {
				tempMidX = currentMidX + currentWidth;
				tempMIdY = currentMidY + currentHeight;
				maxCorrelation = currentCR;
				output.setPixelBold(tempMidX, tempMIdY, Color.green.getRGB());
			}

			// south
			currentCR = cr.getCorrelationValAt(currentMidX, currentMidY
					+ currentHeight);
			if (currentCR > maxCorrelation) {
				tempMidX = currentMidX;
				tempMIdY = currentMidY + currentHeight;
				maxCorrelation = currentCR;
				output.setPixelBold(tempMidX, tempMIdY, Color.green.getRGB());
			}

			// south west
			currentCR = cr.getCorrelationValAt(currentMidX - currentWidth,
					currentMidY + currentHeight);
			if (currentCR > maxCorrelation) {
				tempMidX = currentMidX - currentWidth;
				tempMIdY = currentMidY + currentHeight;
				maxCorrelation = currentCR;
				output.setPixelBold(tempMidX, tempMIdY, Color.green.getRGB());
			}

			// west
			currentCR = cr.getCorrelationValAt(currentMidX - currentWidth,
					currentMidY);
			if (currentCR > maxCorrelation) {
				tempMidX = currentMidX - currentWidth;
				tempMIdY = currentMidY;
				maxCorrelation = currentCR;
				output.setPixelBold(tempMidX, tempMIdY, Color.green.getRGB());
			}

			// west north
			currentCR = cr.getCorrelationValAt(currentMidX - currentWidth,
					currentMidY - currentHeight);
			if (currentCR > maxCorrelation) {
				tempMidX = currentMidX - currentWidth;
				tempMIdY = currentMidY - currentHeight;
				maxCorrelation = currentCR;
				output.setPixelBold(tempMidX, tempMIdY, Color.green.getRGB());
			}

			currentMidX = tempMidX;
			currentMidY = tempMIdY;
			currentHeight = currentHeight / 2;
			currentWidth = currentWidth / 2;
			k--;

		}

		output.setPixelBold(tempMidX, tempMIdY, Color.magenta.getRGB());
		output.label2.repaint();	
		System.out.println("2D Logarithmic Search point (x,y) : (" + currentMidX + "," + currentMidY + ")");

	}

}
