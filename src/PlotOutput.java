import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

public class PlotOutput {
	BufferedImage outImage;
	int width;
	int height;

	JFrame frame;
	JLabel label2;
	JLabel label1;

	public PlotOutput(BufferedImage origianl, double imageArrayMap[][]) {
		width = imageArrayMap[0].length;
		height = imageArrayMap.length;

		outImage = getGrayImage(imageArrayMap);
		showFrame(origianl, outImage);

	}

	public PlotOutput(double originalArrayMap[][], double imageArrayMap[][]) {
		width = imageArrayMap[0].length;
		height = imageArrayMap.length;

		outImage = getGrayImage(imageArrayMap);
		showFrame(getGrayImage(imageArrayMap), outImage);

	}

	public PlotOutput(int originalArrayMap[][], int imageArrayMap[][]) {
		width = imageArrayMap[0].length;
		height = imageArrayMap.length;

		outImage = getGrayImage(imageArrayMap);
		showFrame(getGrayImage(imageArrayMap), outImage);

	}

	public PlotOutput(BufferedImage origianl, BufferedImage modified) {

		outImage = modified;
		showFrame(origianl, modified);

	}

	public BufferedImage getGrayImage(double imageArrayMap[][]) {

		BufferedImage imageGray = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_ARGB);
		for (int i = 0; i < height; i++) {

			for (int j = 0; j < width; j++) {
				int grayC = (int) imageArrayMap[i][j];
				grayC = 0xff000000 | grayC << 16 | grayC << 8 | grayC;
				imageGray.setRGB(j, i, grayC);
			}
		}
		return imageGray;
	}

	public BufferedImage getGrayImage(int imageArrayMap[][]) {

		BufferedImage imageGray = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_ARGB);
		for (int i = 0; i < height; i++) {

			for (int j = 0; j < width; j++) {
				int grayC = imageArrayMap[i][j];
				grayC = 0xff000000 | grayC << 16 | grayC << 8 | grayC;
				imageGray.setRGB(j, i, grayC);
			}
		}
		return imageGray;
	}

	void showFrame(BufferedImage image1, BufferedImage image2) {
		frame = new JFrame("Template Matching");
		frame.setSize(750, 600);
		
		ImageIcon icon1 = new ImageIcon(image1);
		label1 = new JLabel(icon1);
		JScrollPane pane1 = new JScrollPane(label1);
		frame.add(pane1, BorderLayout.WEST);
		pane1.setPreferredSize(new Dimension(frame.getWidth() / 2 - 10, 400));

		ImageIcon icon2 = new ImageIcon(image2);
		label2 = new JLabel(icon2);
		JScrollPane pane2 = new JScrollPane(label2);
		pane2.setPreferredSize(new Dimension(frame.getWidth() / 2 - 10, 400));
		frame.add(pane2, BorderLayout.EAST);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}

	public void setPixelBold(int x, int y, int col) {
		for (int i = x - 1; i <= x + 2; i++) {
			for (int j = y - 1; j <= y + 2; j++) {
				if (i >= 0 && i < width && j >= 0 && j < height) {
					outImage.setRGB(i, j, col);

				}

			}
		}

		label2.repaint();
	}

}
