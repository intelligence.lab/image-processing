import java.awt.Point;

public class CorrTempMatcher {
	int[][] referenceImagePixels;
	int[][] searchImagePixels;
	double[][] correlationImageArray;
	int refimg_Height;
	int refimg_Width;
	int srchimg_Height;
	int srchimg_Width;

	public CorrTempMatcher(int[][] refImgPixels, int[][] srchImgPixels) {

		referenceImagePixels = refImgPixels.clone();
		searchImagePixels = srchImgPixels.clone();
		refimg_Height = referenceImagePixels.length;
		refimg_Width = referenceImagePixels[0].length;
		srchimg_Height = searchImagePixels.length;
		srchimg_Width = searchImagePixels[0].length;
		correlationImageArray = new double[srchimg_Height][srchimg_Width];

	}

	Point getMaxCorrelation() {
		int x = 0;
		int y = 0;
		double max = 0;
		double correlationVal;
		for (int i = 0; i < srchimg_Height; i++) {
			for (int j = 0; j < srchimg_Width; j++) {
				correlationVal = getCorrelationValAt(i, j);
				if (correlationVal > max) {
					max = correlationVal;
					x = j;
					y = i;
				}
			}
		}

		System.out.println("Max Correlation at (x,y) : (" + x + "," + y + ")");
		return new Point(x, y);
	}

	public double getCorrelationValAt(int m, int n) {
		double sum = 0;
		double refSum = 0;
		double srchSum = 0;
		int k, l;
		for (int i = m; i < m + refimg_Height; i++) {
			for (int j = n; j < n + refimg_Width; j++) {
				k = i - m;
				l = j - n;
				if (i < srchimg_Height && j < srchimg_Width) {
					sum += referenceImagePixels[k][l] * searchImagePixels[i][j];
					srchSum += searchImagePixels[i][j]
							* searchImagePixels[i][j];
				}
				refSum += referenceImagePixels[k][l]
						* referenceImagePixels[k][l];
			}
		}

		return sum / (Math.sqrt(refSum * srchSum));
	}
}
