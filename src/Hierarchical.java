import java.awt.Point;


public class Hierarchical {
	
	public String referenceFileName;
	public String testFileName;	
	int maxLevel;
	Point correlationPoint;
	Point testDataDimenstion;
	Point desiredPoint;
	
	public Hierarchical(String ref,String test,int level){
		referenceFileName=ref;
		testFileName=test;
		maxLevel=level;
		try {
			FindImage();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
public void FindImage() throws Exception{

		
		boolean filter=true;
		int magnitude=4;
		
		for(int i=maxLevel;i>=0;i--){
			
			
			
			//if(i==0)filter=false;
			
			int [][]RefData=ImageReader.ReadLevel(i, referenceFileName,filter,magnitude);
			int [][]TestData=ImageReader.ReadLevel(i, testFileName,filter,magnitude);
			
					
			
			
			if(i==maxLevel){
				//Logarithmic logarithmic=new Logarithmic(RefData, TestData);
				//bestPoint=logarithmic.findImage();
				CorrTempMatcher correlation=new CorrTempMatcher(RefData, TestData);
				correlationPoint=correlation.getMaxCorrelation();			
				//testDataDimenstion.setLocation(TestData.length/2, TestData[0].length/2);
				System.out.println(TestData.length/2);
				
				testDataDimenstion.setLocation(TestData.length/2, TestData[0].length/2);
				System.out.println(testDataDimenstion.x);
				System.out.println(testDataDimenstion.getX());
				//testDataDimenstion.y = TestData[0].length/2;
			}
			else{
				
				
				
				int p1=(TestData.length)/2;
				int p2=(TestData[0].length)/2;
				
				int CenterX=p1-1;
				int CenterY=p2-1;
				
				int bestX=correlationPoint.x-testDataDimenstion.x;
				int bestY=correlationPoint.y-testDataDimenstion.y;
				
				CenterX+=2*bestX;
				CenterY+=2*bestY;
				
				//int xLength=Logarithmic.getLength(p1);
				//int yLength=Logarithmic.getLength(p2);
				
				int xLength=RefData.length/2;
				int yLength=RefData[0].length/2;
				
				//System.out.println("size:->"+TestData.length+"\t"+TestData[0].length);
				//System.out.println(CenterX+"\t"+CenterY);
				
				LogSearch logarithmic=new LogSearch(RefData, TestData);
				desiredPoint.x = logarithmic.currentMidX;
				desiredPoint.y = logarithmic.currentMidY;
				//bestPoint=logarithmic.findImage(CenterX, CenterY, xLength, yLength);
				
				//System.out.println(CenterX+"\t"+CenterY);
				
				testDataDimenstion.x=TestData.length/2;
				testDataDimenstion.y=TestData[0].length/2;
			}
			
			
		}
		
		System.out.println("["+correlationPoint.x+"\t"+correlationPoint.y+"]");
		
		//drawRectangle(bestPoint.X,bestPoint.Y);
		
		
	}

}
