import java.awt.Color;
import java.awt.Point;
import java.util.Scanner;

public class Main {

	/**
	 * @param args
	 */

	static ImageReader referenceImage;
	static ImageReader searchImage;
	static CorrTempMatcher correlation;
	static int pointX;
	static int pointY;
	static Point correlationPoint;
	static PlotOutput correlationOutput;
	static int choice;

	public static int[][] getNegitiveImg(int[][] input) {
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input[0].length; j++) {
				input[i][j] = 255 - input[i][j];
			}
		}
		return input;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		referenceImage = new ImageReader("referenceimg.jpg");
		searchImage = new ImageReader("searchimg.jpg");

		System.out.println("1: Correlation based Template Matching");
		System.out.println("2: 2D Logarithmic search");
		System.out.println("3: Hierarchical Search");
		System.out.println("Please enter a choice : ");

		Scanner in = new Scanner(System.in);
		choice = in.nextInt();

		if (choice == 1) {
			correlation = new CorrTempMatcher(referenceImage.getGrayValueMap(),
					searchImage.getGrayValueMap());
			correlationPoint = correlation.getMaxCorrelation();
			pointX = (int) correlationPoint.getX();
			pointY = (int) correlationPoint.getY();
			correlationOutput = new PlotOutput(
					referenceImage.getGrayValueMap(),
					searchImage.getGrayValueMap());
			correlationOutput.setPixelBold(pointX, pointY,
					Color.magenta.getRGB());
		}

		else if (choice == 2) {
			new LogSearch(referenceImage.getGrayValueMap(),
					searchImage.getGrayValueMap());
		}

		else {
			new Hierarchical("referenceimg.jpg",
					"searchimg.jpg",2);
		}
	}

}
