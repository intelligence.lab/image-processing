import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import javax.imageio.ImageIO;

public class ImageReader {
	public BufferedImage image, grayImage;
	private final float redModifier = (float) 0.298;
	private final float greenModifier = (float) 0.587;
	private final float blueModifier = (float) 0.114;
	public int height, width;
	public int pixelValue[][];

	public ImageReader(String imageName) {
		try {
			File input = new File(imageName);
			image = ImageIO.read(input);
			height = image.getHeight();
			width = image.getWidth();
			// System.out.print("width:" + width + " height:" + height);

			pixelValueMap(image);
			// showFrame(image,imageGray);
		} catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
		}
	}
	
	
	public static int [][] readColorImageDataFromBufferedImage(BufferedImage image) throws Exception
	{
		int w = image.getWidth();
        int h = image.getHeight();

	       int [][]original = new int[h][w];

	        
	        for(int i = 0 ; i < h ; i++)        	
	            for(int j = 0 ; j < w ; j++)
	            {
	            	
	            	int rgb = image.getRGB(j, i); 
	     	       
	            	int red = (rgb>>16)&0x0ff;
	            	int green=(rgb>>8) &0x0ff;
	            	int blue= (rgb>>0) &0x0ff;
	            	int alpha=(rgb>> 24)&0x0ff;
	            	
	                original[i][j] = (red+green+blue)/3;
	            }
	       
     return original;                 
	}
	
	
	public static int [][] readColorImageData(String path) throws Exception
    {
        File file = new File(path);// file object to get the file, the second argument is the name of the image file
        BufferedImage image = ImageIO.read(file);
        return readColorImageDataFromBufferedImage(image);
    }  
	
	

	public BufferedImage getGrayImage() {
		grayImage = new BufferedImage(width, height,
				BufferedImage.TYPE_BYTE_GRAY);
		getGrayImage(image, grayImage);
		return grayImage;
	}

	public void pixelValueMap(BufferedImage in) {

		pixelValue = new int[height][width];
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int pixel = in.getRGB(j, i);
				int red = (pixel >> 16) & 0xff;
				int green = (pixel >> 8) & 0xff;
				int blue = (pixel) & 0xff;
				int gray = (int) (redModifier * red + greenModifier * green + blueModifier
						* blue);
				pixelValue[i][j] = gray;

			}
		}

	}

	public int[][] getGrayValueMap() {
		return pixelValue;
	}

	public int[][] getGrayImage(BufferedImage in, BufferedImage out) {

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int grayC = 0xff000000 | pixelValue[i][j] << 16
						| pixelValue[i][j] << 8 | pixelValue[i][j];
				out.setRGB(j, i, grayC);
			}

		}
		return pixelValue;
	}

	public static BufferedImage scale(BufferedImage sbi, int imageType,
			int dWidth, int dHeight, double fWidth, double fHeight) {
		BufferedImage dbi = null;
		if (sbi != null) {
			dbi = new BufferedImage(dWidth, dHeight, imageType);
			Graphics2D g = dbi.createGraphics();
			AffineTransform at = AffineTransform.getScaleInstance(fWidth,
					fHeight);
			g.drawRenderedImage(sbi, at);
		}
		return dbi;
	}

	public static BufferedImage lowPassFilterTheImage(BufferedImage image,
			int blurMagnitude) throws Exception {

		float[] kernel = new float[blurMagnitude * blurMagnitude];
		for (int i = 0, n = kernel.length; i < n; i++) {
			kernel[i] = 1f / n;
		}

		ConvolveOp blur = new ConvolveOp(new Kernel(blurMagnitude,
				blurMagnitude, kernel), ConvolveOp.EDGE_NO_OP, null);
		BufferedImage buffImage = new BufferedImage(image.getWidth(),
				image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		buffImage = blur.filter(image, buffImage);

		return buffImage;
	}

	public static int[][] ReadLevel(int i, String imageName, boolean filter,
			int magnitude) throws Exception {

		File file = new File(imageName);
		BufferedImage image = ImageIO.read(file);

		if (filter)
			image = lowPassFilterTheImage(image, magnitude);

		// disPlay(image);

		// System.exit(0);

		double Level = Math.pow(2, i);

		int height = (int) (image.getHeight() / Level);
		int width = (int) (image.getWidth() / Level);

		BufferedImage modifiedImage = scale(image, BufferedImage.TYPE_INT_ARGB,
				width, height, 1 / Level, 1 / Level);

		// disPlay(modifiedImage);

		return readColorImageDataFromBufferedImage(modifiedImage);

	}

}
